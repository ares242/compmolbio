set xtic auto	                          # set xtics automatically
set ytic auto                          # set ytics automatically
#set ylabel "Tiempo (secs)"
#set xlabel "Nro. Imágenes"
set grid
#set xrange [0:20]
fileName = "dna.out"
set term pngcairo size 1024,768 enhanced font 'Verdana,10'
set output "fig.png"
#set title "Comparacion metodos de paralelizacion del algoritmo de la simetria de imagenes"
#title 'Lineal' with linespoints,fileName using 1:3
#plot fileName using 1:2:3 title 'Lineal' with points pt 7 ps 1 lt palette #,fileName using 1:2 title 'Rotaciones' with linespoints ,fileName using 1:4 title 'XOR' with linespoints ,fileName using 1:5 title 'Rotaciones + XOR' with linespoints
plot fileName using 1:2 title 'Convergencia' with points #,fileName using 1:2 title 'Rotaciones' with linespoints ,fileName using 1:4 title 'XOR' with linespoints ,fileName using 1:5 title 'Rotaciones + XOR' with linespoints
#plot fileName using 1:2 title 'Rotaciones' with linespoints ,fileName using 1:3 title 'XOR' with linespoints ,fileName using 1:4 title 'Rotaciones + XOR' with linespoints
#plot fileName using 1:2 title 'Rotaciones' with linespoints ,fileName using 1:3 title 'XOR 2D' with linespoints ,fileName using 1:4 title 'XOR 1D' with linespoints ,fileName using 1:5 title 'Rotaciones + XOR 2D' with linespoints
set term wxt enhanced font 'Verdana,10' persist
replot

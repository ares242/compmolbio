#include <iostream>
#include <fstream>
#include <bitset>

#include "MaxSquare/MaxSquare.h"
#include "Gauss/Gauss.h"
#include "mDNAalign/mDNAalignGA.h"

#include "mDNAalign/alignutil.h"

using namespace std;

/*ostream & operator<<(ostream & os, GapCromosome gaps)
{
    os<<"[ ";
    for(auto & iter :gaps)
    {
        os<<'('<<iter.first<<','<<iter.second<<')'<<' ';
    }
    os<<"]"<<std::endl;

    return os;
}*/

class DNAReader
{
    public:
        static std::vector<std::string>* getSeqs(size_t num_seqs)
        {
            std::vector<std::string>* seqs=new std::vector<std::string>(num_seqs);
            for(size_t i=0;i<num_seqs;i++)
                (*seqs)[i]=randomDNAchain();
            return seqs;
        }

        static std::vector<std::string>* getSeqs(string fname)
        {
            ifstream dnafile(fname);
            std::vector<std::string>* seqs=new std::vector<std::string>();

            string newdna;
            string str;

            while(std::getline(dnafile,str))
            {
                if(str[0]!='>')
                    newdna+=str;
                else
                {
                    if(newdna.size()>0)
                        (*seqs).push_back(newdna);
                    newdna="";
                }
            }
            (*seqs).push_back(newdna);

            dnafile.close();
            return seqs;
        }
};

int main()
{
	omp_set_num_threads(8);
    cout<<"Multi DNA sequence aligment..."<<endl;
    //MaxSquare m(10,500);
    //m.exec();
    //cout<<exp(2)<<endl;
    //cout<<exp(-1.4*1.4)*exp(-1.0*1.0)<<endl;
    //double a = 0.244555;
    //cout<<convert(convert(a))<<endl;
    //Gauss g(50,100);
    //g.exec();
    /*unordered_map<size_t,char> c;
    for(int i = 0; i< 100; i++)
        cout<<c[i]<<endl;*/

    /*cout<<b1.to_string()<<endl;
    cout<<b2.to_string()<<endl;
    auto c1( (b1<<4>>4) | (b2>>4<<4) );
    auto c2( (b1>>4<<4) | (b2<<4>>4) );
    cout<<c1.to_string()<<endl;
    cout<<c2.to_string()<<endl;*/

    /*size_t num_seqs = 20;

    std::vector<std::string> seqs(num_seqs);
    for(size_t i = 0; i<num_seqs;i++)
    {
        seqs[i] =  randomDNAchain();
    }*/
    /*
    mDNAalignIndividual test(seqs,6);
    test.SetInitial();
    size_t offset = 0;
    for(int i =0 ; i<12; i++)
    cout<<(char)test.seqAt(0,i,offset)<<endl;
    */
    //mDNAalign dna("stats/dna.out",100,5000,seqs);
    //mDNAalign dna(50,100,*DNAReader::getSeqs(10));
	mDNAalign dna("stats/dna.out",100,100,*DNAReader::getSeqs("microbial.medium.fna"));
    dna.Setpmutation(0.002);
    dna.Setpcrossover(0.3);
    dna.exec();
	cout<<"Imprimiendo alineamientos"<<endl;
    ofstream aligment("align.dna");
    dna.printOut(aligment);

    /*string str1 = "AACAATTCCGACTAC";

    str1.insert(str1.size()-1,"_");
    cout<<str1<<endl;*/

    /*string str1 = "AACAATTCCGACTAC";//"CAGCACTTGGATTCTCGG";//"ACGTCATCA";//randomDNAchain(20);
    string str2 = "ACTACCTCGC";//"CAGCGTGG";//"TAGTGTCA";//randomDNAchain(20);
    IntImg img;

    int score = align(str1,str2,img);


    string a_str1,a_str2;
    GapCromosome gaps1,gaps2;
    traceback(str1,str2,img,a_str1,a_str2);
    //traceback_gaps(str1,str2,img,gaps1,gaps2);
    cout<<str1<<" "<<str1.size()<<endl;
    cout<<str2<<" "<<str2.size()<<endl<<endl;

    cout<<a_str1<<endl;
    cout<<a_str2<<endl<<endl;

    cout<<gaps1<<endl;
    cout<<gaps2<<endl;*/

    cout<<"DONE"<<endl;
    return 0;
}

#include "GeneticAlgorithm.h"


GeneticAlgorithm::GeneticAlgorithm(size_t pop_size ,size_t iters,AbstractIndividualFactory * indivFactory)
:m_old(Population(pop_size,indivFactory)),m_new(Population(pop_size,indivFactory)),num_iters(iters)
{
    //ctor
}

GeneticAlgorithm::~GeneticAlgorithm()
{

}

void GeneticAlgorithm::exec()
{
    std::cout<<"Generando ...\n";
    m_old.generate();
    std::cout<<"Calc probs...\n";
    m_old.calcProbs();
    printPopulation(0);
    for(size_t i=0;i<num_iters;i++)
    {
        std::cout<<"Seleccionando ...\n";
        Selection();
        std::cout<<"Cruzando ...\n";
        CrossOver();
        std::cout<<"Mutando ...\n";
        Mutation();
        m_old = m_new;
        m_old.calcProbs();
        std::cout<<"Imprimiendo...\n";
        printPopulation(i+1);
    }
}

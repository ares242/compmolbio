#include "Population.h"


Population::Population(std::size_t size,AbstractIndividualFactory * indivFactory)
:m_individuals(std::vector<AbstractIndividual*>(size))
,m_acumProb(std::vector<double>(size))
,m_individualFactory(indivFactory)
{
}

Population::~Population()
{
    //dtor
}

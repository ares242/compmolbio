#include "common.h"

double convert(doubleBitSet const& bs)
{
    Converter c;
    c.i = bs.to_ullong();
    return c.d;
}

doubleBitSet convert(double d1)
{
    return doubleBitSet(*reinterpret_cast<unsigned long*>(&d1));
}

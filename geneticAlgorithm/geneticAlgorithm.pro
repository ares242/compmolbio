TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    src/common.cpp \
    src/GeneticAlgorithm.cpp \
    src/Population.cpp

HEADERS += \
    include/AbstractCromosome.h \
    include/AbstractIndividual.h \
    include/AbstractIndividualFactory.h \
    include/common.h \
    include/GeneticAlgorithm.h \
    include/Population.h \
    include/Gauss/Gauss.h \
    include/Gauss/GaussIndividual.h \
    include/MaxSquare/MaxSquare.h \
    include/MaxSquare/MaxSquareCromosome.h \
    include/MaxSquare/MaxSquareIndiv.h \
    include/MaxSquare/MaxSquareIndividualFactory.h \
    include/mDNAalign/alignutil.h \
    include/mDNAalign/mDNAalignGA.h

INCLUDEPATH += include/

#LIBS += -lX11 -lpthread -ltbb -lrt -lboost_system -lboost_filesystem

LIBS += -ltbb

QMAKE_CXXFLAGS += -std=c++0x

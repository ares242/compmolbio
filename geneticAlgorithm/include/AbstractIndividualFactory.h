#ifndef ABSTRACTINDIVIDUALFACTORY_H
#define ABSTRACTINDIVIDUALFACTORY_H

#include "AbstractIndividual.h"

class AbstractIndividualFactory
{
    public:
        virtual AbstractIndividual * createIndividual() = 0;
    protected:
    private:
};

#endif // ABSTRACTINDIVIDUALFACTORY_H

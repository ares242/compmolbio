#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include "Population.h"
#include <random>
#include "common.h"
#include "AbstractIndividualFactory.h"

class GeneticAlgorithm
{
    public:
        GeneticAlgorithm(size_t pop_size = 0,size_t iters=0,AbstractIndividualFactory * indivFactory = nullptr);
        virtual ~GeneticAlgorithm();
        Population Getold() { return m_old; }
        void Setold(Population val) { m_old = val; }
        Population Getnew() { return m_new; }
        void Setnew(Population val) { m_new = val; }
        float Getpselection() { return m_pselection; }
        void Setpselection(float val) { m_pselection = val; }
        float Getpmutation() { return m_pmutation; }
        void Setpmutation(float val) { m_pmutation = val; }
        float Getpcrossover() { return m_pcrossover; }
        void Setpcrossover(float val) { m_pcrossover = val; }
        size_t GetNum_iters(){return num_iters;}
        void SetNum_iters(size_t val){num_iters = val;}

        AbstractIndividualFactory * getIndividualFactory() {return m_individualFactory;}
        void setIndividualFactory(AbstractIndividualFactory * val) {m_individualFactory = val;}

        void exec();

    protected:

        virtual void Selection()
        {
            /*std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            std::vector<AbstractIndividual*> tmp(old.size()*2);
            for(size_t i = 0 ; i<tmp.size();i++)
            {
                AbstractIndividual* indiv = m_old.SelectIndividual(prob_dis(gen));
                tmp[i] = indiv;
            }
            old = tmp;*/

		    std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            std::vector<AbstractIndividual*> tmp(old.size()*2);

            tbb::parallel_for(tbb::blocked_range<size_t>(0,tmp.size()),[&](const tbb::blocked_range<size_t>& r)
            {
                for(size_t i=r.begin();i!=r.end();++i)
                {
                    AbstractIndividual* indiv = m_old.SelectIndividual(prob_dis(gen));
                    tmp[i] = indiv;
                }
            });
            old = tmp;
        }

        //virtual T binary_cross(const T& a,const T& b) = 0;

        virtual void CrossOver()
        {
            std::vector<AbstractIndividual*>& _new=m_new.Getindividuals();
            //size_t offset = m_pcrossover*_new.size();

            tbb::parallel_for( tbb::blocked_range<size_t>(0,_new.size()),
                          [&](const tbb::blocked_range<size_t>& r) {
                for(size_t i=r.begin(); i!=r.end(); ++i)
                {
                    if(prob_dis(gen)<m_pcrossover)
                        _new[i] = m_old.SelectIndividual(prob_dis(gen))->cross(m_old.SelectIndividual(prob_dis(gen)));
                                //old[i*2]->cross(old[i*2+1]);
                    else
                        _new[i] = m_old.SelectIndividual(prob_dis(gen));
                }
            }
            );

            /*for(size_t i = 0 ; i < _new.size() ;i++)
            {
                if(prob_dis(gen)<m_pcrossover)
                    _new[i] = m_old.SelectIndividual(prob_dis(gen))->cross(m_old.SelectIndividual(prob_dis(gen)));
                            //old[i*2]->cross(old[i*2+1]);
                else
                    _new[i] = m_old.SelectIndividual(prob_dis(gen));
                            //(old[i*2]->Getfitness()>old[i*2+1]->Getfitness())?old[i*2]:old[i*2+1];
            }*/
           // for(size_t i = offset; i<_new.size();i++)
                //_new[i] = old[i*2];
        }

        virtual void Mutation()
        {
            /*for( AbstractIndividual * ind : m_old.Getindividuals())
            {
                if(prob_dis(gen)<m_pmutation)
                    ind->mutate();
            }*/

            std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            tbb::parallel_for(tbb::blocked_range<size_t>(0,old.size()),[&](const tbb::blocked_range<size_t>& r)
            {
              for(size_t i=r.begin();i!=r.end();++i)
              {
                if(prob_dis(gen)<m_pmutation)
                    old[i]->mutate();
              }
            });
        }

        virtual AbstractIndividual* getResult(){
            if(m_old.Getindividuals().empty()) return nullptr;
            double max_fitness = m_old.Getindividuals()[0]->Getfitness();
            AbstractIndividual * res = 0;
            for( AbstractIndividual * ind : m_old.Getindividuals())
            {
                if(max_fitness<=ind->Getfitness())
                {
                    max_fitness = ind->Getfitness();
                    res = ind;
                }
            }
            return res;
        }

        virtual void printPopulation(size_t)
        {

        }

        Population m_old;
        Population m_new;

        float m_pselection;
        float m_pmutation;
        float m_pcrossover;
        size_t num_iters;
        AbstractIndividualFactory * m_individualFactory;

    private:
};

#endif // GENETICALGORITHM_H

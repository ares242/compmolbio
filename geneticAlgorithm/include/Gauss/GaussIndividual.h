#ifndef GAUSSINDIVIDUAL_INCLUDED
#define GAUSSINDIVIDUAL_INCLUDED

#include <iostream>

#define floatcromosize 64

static std::uniform_real_distribution<> real_dis(-5,5);
static std::uniform_int_distribution<> cross_dis(0,63);

class GaussCromosome: public AbstractCromosome
{
    public:
    GaussCromosome(){}
    GaussCromosome(doubleBitSet data)
        :m_data(data)
        {}

        virtual ~GaussCromosome() {}

        void setData(doubleBitSet val){m_data=val;}
        doubleBitSet getData(){ return m_data;}

        AbstractCromosome* cross(AbstractCromosome* const & other)
        {
            size_t offset=cross_dis(gen);
            //std::cout<<offset<<" ";
            return new GaussCromosome( (m_data<<offset>>offset) | ( dynamic_cast<GaussCromosome*>(other)->m_data>>offset<<offset) );
        }

    protected:
    private:
        doubleBitSet m_data;
};

class GaussIndividual :public AbstractIndividual
{
public:
    GaussIndividual()
    :AbstractIndividual(2)
    {}
    virtual ~GaussIndividual(){}

    double evalFitness()
    {
        doubleBitSet x_ = (dynamic_cast<GaussCromosome*>(m_cromosomes[0]))->getData();
        doubleBitSet y_ = (dynamic_cast<GaussCromosome*>(m_cromosomes[1]))->getData();

        double x = convert(x_);
        double y = convert(y_);
        double res = exp((-(x*x+y*y))/2.0);
        //std::cout<<x<<"\t"<<y<<"\t";
        //double res = exp(-x*x)*exp(-y*y);
        //std::cout<<res<<std::endl;
        return res;
    }

    void SetInitial()
    {
        double d1 = real_dis(gen);
        double d2 = real_dis(gen);
          m_cromosomes[0] = new GaussCromosome(convert(d1));
          m_cromosomes[1] = new GaussCromosome(convert(d2));
        //std::cout<<d1<<"\t\t"<<d2<<std::endl;
    }
    void mutate(){}
};


#endif // GAUSSINDIVIDUAL_INCLUDED

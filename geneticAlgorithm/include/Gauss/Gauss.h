#ifndef GAUSS_H
#define GAUSS_H

#include "GeneticAlgorithm.h"
#include "GaussIndividual.h"
#include "common.h"

class GaussIndividualFactory : public AbstractIndividualFactory
{
    public:
        GaussIndividualFactory() {}
        virtual ~GaussIndividualFactory() {}

        AbstractIndividual * createIndividual()
        {
            return new GaussIndividual;
        }

    protected:
    private:
};

class Gauss : public GeneticAlgorithm
{
    public:
        Gauss() {}
        Gauss(size_t pop_size = 0,size_t iters=0)
        :GeneticAlgorithm(pop_size,iters,new GaussIndividualFactory)
        {}

        virtual ~Gauss() {}
    protected:
        void printPopulation(size_t generation)
        {
            std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            for(AbstractIndividual* & i:old)
            {
                GaussIndividual* indiv = dynamic_cast<GaussIndividual*>(i);
                GaussCromosome * cromo1 = dynamic_cast<GaussCromosome*> (indiv->Getcromosomes()[0]);
                GaussCromosome * cromo2 = dynamic_cast<GaussCromosome*> (indiv->Getcromosomes()[1]);
                std::cout<< convert(cromo1->getData())<<"\t"<<convert(cromo2->getData())<<"\t"<<indiv->Getfitness()<<"\t"<<generation<<std::endl;
            }
        }
    private:
};

#endif // GAUSS_H

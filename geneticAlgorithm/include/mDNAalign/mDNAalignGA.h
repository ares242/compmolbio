 #ifndef MDNAALIGNGA_H_INCLUDED
#define MDNAALIGNGA_H_INCLUDED

#include "alignutil.h"
#include <iostream>

class mDNAalignCromosome: public AbstractCromosome
{
    public:
    mDNAalignCromosome(){}
    mDNAalignCromosome(GapCromosome data)
        :m_data(data)
        {}

        virtual ~mDNAalignCromosome() {}

        void setData(GapCromosome val){m_data=val;}
        GapCromosome & getData(){ return m_data;}
        AbstractCromosome* cross(AbstractCromosome* const & /*other*/)
        {
            /*size_t offset=cross_dis(gen);
            //std::cout<<offset<<" ";
            return new mDNAalignCromosome( (m_data<<offset>>offset) | ( dynamic_cast<mDNAalignCromosome*>(other)->m_data>>offset<<offset) );
            */
            return nullptr;
        }

    protected:
    private:
        std::string m_data;
};

class mDNAalignIndividual :public AbstractIndividual
{
public:
    mDNAalignIndividual(std::vector<std::string> & seqs,size_t max_s)
    :AbstractIndividual(seqs.size()),sequences(seqs),position_dist(0,seqs.size()-1),max_size(max_s)
    {}
    virtual ~mDNAalignIndividual(){}

    size_t getMaxSize(){return max_size;}

    inline double evalSPscore(char const & a, char const & b)
    {
        //if both are gaps
        //if(a=='_' && b=='_') return 0;
        if(a=='_' || b=='_') return 1;


        //if are not gaps
        if(a==b) return 6;
        if(a!=b) return 1;

        return 0;
    }

    double columnScore(std::vector<int> & column)
    {
        double total = 0;
        int row_val;
        for(size_t i = 1; i< column.size(); i++)
        {
            row_val = column[i];
            for(size_t j = 0;j<i; j++)
            {
                total += evalSPscore(row_val,column[j]);
            }
        }
        return total;
    }

    double pairScore(std::string & a,std::string & b)
    {
        double total = 0;
        for(size_t i = 1; i< a.size(); i++)
        {
            total += evalSPscore(a[i],b[i]);
        }
        return total;
    }

    double evalFitness()
    {
        double total_score = 0;
        for(size_t i = 1; i < sequences.size(); i++)
        {   
            for(size_t j = 0; j <i; j++)
            {
                total_score += pairScore( static_cast<mDNAalignCromosome*>(m_cromosomes[i])->getData()
                                          ,static_cast<mDNAalignCromosome*>(m_cromosomes[j])->getData());
            }
        }
        return total_score;
    }



    void SetInitial()
    {   
        for(size_t i = 0 ; i< sequences.size(); i++)
        {
            mDNAalignCromosome* cromo;
            m_cromosomes[i] = cromo = new mDNAalignCromosome(sequences[i]);
            std::string & str = cromo->getData();
            if(max_size!=str.size())
            {
                size_t dif = max_size-str.size();
                for(size_t j = 0; j<dif; j++)
                {
                    std::uniform_int_distribution<size_t> p_dis(0,str.size()-1);
                    str.insert(p_dis(gen),"_");
                }
            }
        }
        //assert(verify_size());
    }

    bool verify_size()
    {
        for( AbstractCromosome * & cromo : m_cromosomes )
        {
            std::string & str = ((mDNAalignCromosome*) cromo )->getData();
            if(max_size!=str.size())
            {
                std::cout<<"SIZE ERROR : "<<str.size()<<'\t'<<max_size<<std::endl;
                return false;
            }
        }
        return true;
    }

    void complete(size_t & _max_size, std::string & _str, std::vector<AbstractCromosome*>& cromos)
    {
        size_t str_s = _str.size();
        if(_max_size==str_s) return;
        if(_max_size>str_s)
        {
            long diff = abs(_max_size-str_s);
            _str.insert(str_s,diff,'_');
        }
        else if(_max_size<str_s)
        {
            _max_size=str_s;
            for( AbstractCromosome * & cromo : cromos )
            {
                std::string & str = ((mDNAalignCromosome*) cromo )->getData();
                if(str.size() == _max_size) continue;
                long diff = abs(str.size()-_max_size);
                str.insert(str.size(),diff,'_');
            }
        }
        //assert(ind->verify_size());
    }

    virtual AbstractIndividual* cross(AbstractIndividual* other)
    {
        ///TODO: verficar m_cromosomes protected
        size_t pos = position_dist(gen);
        std::swap(m_cromosomes[pos],other->m_cromosomes[pos]);
        std::string & s1 = ((mDNAalignCromosome* )(m_cromosomes[pos]))->getData();
        std::string & s2 = ((mDNAalignCromosome* )(other->m_cromosomes[pos]))->getData();
        complete(max_size,s1,m_cromosomes);
        complete(((mDNAalignIndividual* )other)->max_size,s2,other->m_cromosomes);
        return this;
    }

    void mutate()
    {
        std::uniform_int_distribution<size_t> mut_dis(0,max_size-1);
        for(AbstractCromosome * & cromo :m_cromosomes)
        {
            int p = mut_dis(gen);
            ((mDNAalignCromosome*) cromo)->getData().insert(p,"_");
        }
        max_size++;
    }

private:
    std::vector<std::string> & sequences;
    std::uniform_int_distribution<size_t> position_dist;
    size_t max_size;
};

class mDNAalignIndividualFactory : public AbstractIndividualFactory
{
    public:
        mDNAalignIndividualFactory(std::vector<std::string> const & seqs)
        :sequences(seqs)
        {
            max_size = 0;
            for(std::string & s : sequences)
                max_size = (s.size()>max_size)?s.size():max_size;
        }
        virtual ~mDNAalignIndividualFactory() {}

        AbstractIndividual * createIndividual()
        {
            return new mDNAalignIndividual(sequences, max_size);
        }

    protected:
    private:
        std::vector<std::string> sequences;
        size_t max_size;
};

class mDNAalign : public GeneticAlgorithm
{
    public:
        mDNAalign(std::string logfile,size_t pop_size = 0,size_t iters=0,std::vector<std::string> const & _data = std::vector<std::string>())
        :GeneticAlgorithm(pop_size,iters,new mDNAalignIndividualFactory(_data)),log(logfile)
        {}

        virtual ~mDNAalign() {log.close();}

        void printOut(std::ostream & os)
        {

            mDNAalignIndividual* ind=dynamic_cast<mDNAalignIndividual*>(this->getResult());
            for(AbstractCromosome* & cromo :ind->Getcromosomes())
            {
                os<<static_cast<mDNAalignCromosome*>(cromo)->getData()<<std::endl;
            }
        }

    protected:
        void printPopulation(size_t generation)
        {
            //mut_dis.reset();
            std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            for(AbstractIndividual* & i:old)
            {
                //std::cout<<generation <<"\t"<< i->Getfitness()<<"\t"<<((mDNAalignIndividual*)i)->getMaxSize()<<std::endl;
                log<<generation <<"\t"<< i->Getfitness()<<"\t"<<((mDNAalignIndividual*)i)->getMaxSize()<<std::endl;
            }
        }

    private:
        std::ofstream log;
};

#endif // MDNAALIGNGA_H_INCLUDED

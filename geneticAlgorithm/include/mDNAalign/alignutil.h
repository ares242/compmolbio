#ifndef ALIGNUTIL_H
#define ALIGNUTIL_H

#include <string>
#include <sstream>
#include <unordered_map>
#include <ostream>
#include <omp.h>
#include <tbb/tbb.h>

//typedef std::unordered_map<size_t,size_t> GapCromosome;
typedef std::string GapCromosome;

template<typename T>
class matrix2d
{
public:
    matrix2d(size_t width=0,size_t height=0,T* data = nullptr)
    :_width(width),_height(height),_data(data)
    {
    }
    inline void assign(size_t width=0,size_t height=0)
    {
        _width = width;
        _height = height;
        _data =  new T[width*height];
    }
    void fill(T val)
    {
        T* iter = _data;
        for(size_t d_i = 0; d_i<(_width*_height); d_i++,iter++)
            (*iter) = val;
    }
    T & operator()(size_t const & x, size_t const & y)
    {
        return _data[y*_width+x];
    }
    size_t width(){return _width;}
    size_t height(){return _height;}
private:
    size_t _width;
    size_t _height;
    T* _data;
};

typedef matrix2d<float> IntImg;
typedef matrix2d<unsigned char> Img;

template<class T>
std::ostream & operator<<(std::ostream & os, matrix2d<T> img)
{
    os<<'('<<img.width()<<'x'<<img.height()<<')'<<std::endl;
    for(size_t i = 0; i<img.height(); i++)
    {
        for(size_t j = 0; j<img.width(); j++)
        {
            os<<img(j,i)<<"\t";
        }
        os<<std::endl;
    }
    return os;
}

template <typename T>
inline T max(const T& a,const T& b,const T& c)
{
    return std::max(std::max(a,b),c);
}

inline int isMatch(char& ch1, char& ch2)
{
    return (ch1==ch2)?1:-1;
}

static char dna_chars [] = "AGTC";
static std::uniform_int_distribution<> prot_dis(0, 3);
static std::uniform_int_distribution<> size_dis(10, 100);
std::string randomDNAchain(size_t size_ = size_dis(gen))
{
    std::string out;
    out.resize(size_);
    for(size_t i = 0; i<size_;i++)
        out.at(i)=dna_chars[prot_dis(gen)];
    return out;
}


int align(std::string& str1, std::string& str2, IntImg& img)
{
    size_t length1 = str1.size()+1;
    size_t length2 = str2.size()+1;
    img.assign(length1,length2);
    img.fill(0);
    size_t str1_i, str2_i;
    for(str1_i = 0; str1_i<length1;str1_i++)
        img(str1_i,0) = ((int)str1_i)*(-2);

    for(str2_i = 0; str2_i<length2;str2_i++)
        img(0,str2_i) = ((int)str2_i)*(-2);

    for(str1_i = 1; str1_i<length1; str1_i++)
    {
        for(str2_i = 1; str2_i<length2; str2_i++)
        {
            img(str1_i,str2_i) = max(img(str1_i-1,str2_i)-2,
                                    img(str1_i,str2_i-1)-2,
                                    img(str1_i-1,str2_i-1)+isMatch(str1[str1_i-1],str2[str2_i-1]));
        }
    }
    return img(length1-1,length2-1);
}

void traceback(std::string const & str1 ,std::string const & str2,IntImg & img, std::string & out1, std::string & out2)
{
    size_t img_i, img_j;
    img_i = img.width()-1;
    img_j = img.height()-1;
    //size_t maxsize = max(img_i,img_j);
    std::stringstream _out1;
    std::stringstream _out2;
    _out1<<str1[img_i-1];
    _out2<<str2[img_j-1];
    while(img_i>0 && img_j>0)
    {
        int diag = img(img_i-1,img_j-1);
        int left = img(img_i-1,img_j);
        int upper = img(img_i,img_j-1);
        if(diag>=left && diag>=upper)
        {
            _out1<<str1[img_i-2];
            _out2<<str2[img_j-2];
            img_i--,img_j--;
        }
        else if(left>=upper)
        {
            _out1<<str1[img_i-2];
            _out2<<'_';
            img_i--;
        }
        else
        {
            _out1<<'_';
            _out2<<str2[img_j-2];
            img_j--;
        }
    }

    out1 = _out1.str();
    out2 = _out2.str();
    reverse(out1.begin(),out1.end());
    reverse(out2.begin(),out2.end());
}


/*void traceback_gaps(std::string & str1, std::string & str2, IntImg & img, GapCromosome & out1, GapCromosome & out2)
{
    size_t img_i, img_j;
    img_i = img.width()-1;
    img_j = img.height()-1;
    //size_t maxsize = max(img_i,img_j);
    while(img_i>0 && img_j>0)
    {
        int diag = img(img_i-1,img_j-1);
        int left = img(img_i-1,img_j);
        int upper = img(img_i,img_j-1);
        if(diag>=left && diag>=upper)
        {
            img_i--,img_j--;

        }
        else if(left>=upper)
        {
            out2[img_j-1]++;
            img_i--;
        }
        else
        {
            out1[img_i-1]++;
            img_j--;
        }
    }
}*/

#endif //ALIGNUTIL_H

#ifndef POPULATION_H
#define POPULATION_H

#include <vector>
#include <algorithm>
#include "AbstractIndividual.h"
#include "AbstractIndividualFactory.h"
#include <tbb/tbb.h>

class Population
{
public:
    Population(std::size_t size=0,AbstractIndividualFactory * indivFactory = 0);
    virtual ~Population();
    float Getfitnesstotal() { return m_fitnesstotal; }
    void Setfitnesstotal (float val) {m_fitnesstotal = val; }

    std::vector<AbstractIndividual*>& Getindividuals() { return m_individuals; }
    void Setindividuals(std::vector<AbstractIndividual*> val) { m_individuals = val; }
    std::vector<double>& GetAcumProbs() { return m_acumProb; }
    void SetAcumProbs(std::vector<double> val) { m_acumProb = val; }

    AbstractIndividualFactory * GetIndividualFactory(){return m_individualFactory;}
    void GetIndividualFactory(AbstractIndividualFactory * val){m_individualFactory = val;}

    ///TODO: Optimize search in intervals
    AbstractIndividual* SelectIndividual(double prob)
    {
        double last = 0;
        for(size_t i = 0; i<m_acumProb.size(); i++)
        {
            if(prob>last && prob <= m_acumProb[i])
            {
                return m_individuals[i];
            }
            else
                last = m_acumProb[i];
        }
        std::cout<<prob<<"\tNo selection!"<<std::endl;
        return nullptr;
    }

    void calcProbs()
    {
        double prob_total = tbb::parallel_reduce(
                tbb::blocked_range<size_t>(0, m_individuals.size()),
                0.f,
                [&](const tbb::blocked_range<size_t>& r, double init)->double {
                    for(size_t i=r.begin(); i!=r.end(); ++i)
                    {
                        AbstractIndividual * & ind = m_individuals[i];
                        ind->Setfitness(ind->evalFitness());
                        init += ind->Getfitness();
                    }
                    return init;
                },
                []( double x, double y )->double {
                    return x+y;
                }
            );

        /*for(AbstractIndividual* & i:m_individuals)
        {
            i->Setfitness(i->evalFitness());
            prob_total += i->Getfitness();
        }*/

        tbb::parallel_for( tbb::blocked_range<size_t>(0,m_individuals.size()),
                      [&](const tbb::blocked_range<size_t>& r) {
            for(size_t i=r.begin(); i!=r.end(); ++i)
            {
                AbstractIndividual * & ind = m_individuals[i];
                ind->Setpselection(ind->Getfitness()/prob_total);
            }
        }
        );

        double acum = 0;
        for(size_t ind_i=0;ind_i<m_acumProb.size();ind_i++)
        {
            acum += m_individuals[ind_i]->Getpselection();
            m_acumProb[ind_i] = acum;
        }
    }

    void generate()
    {

        tbb::parallel_for( tbb::blocked_range<size_t>(0,m_individuals.size()),
                      [&](const tbb::blocked_range<size_t>& r) {
            for(size_t i=r.begin(); i!=r.end(); ++i)
            {
                m_individuals[i] = m_individualFactory->createIndividual();
                m_individuals[i]->SetInitial();
            }
        }
        );
    }

protected:
private:
    float m_fitnesstotal;
    std::vector<AbstractIndividual*> m_individuals;
    std::vector<double> m_acumProb;
    AbstractIndividualFactory * m_individualFactory;
};
#endif // POPULATION_H

#ifndef MAXSQUARECROMOSOME_H
#define MAXSQUARECROMOSOME_H

#include "AbstractCromosome.h"

#define cromosize 8
#include <bitset>

class MaxSquareCromosome : public AbstractCromosome
{
    public:
        MaxSquareCromosome()
        {}

        MaxSquareCromosome(std::bitset<cromosize> data)
        :m_data(data)
        {}

        virtual ~MaxSquareCromosome() {}

        void setData(std::bitset<cromosize> val){m_data=val;}
        std::bitset<cromosize> getData(){ return m_data;}

        AbstractCromosome * cross(AbstractCromosome * const & other)
        {
            size_t offset = cromosize / 2;
            return new MaxSquareCromosome( (m_data<<offset>>offset) | ( dynamic_cast<MaxSquareCromosome*>(other)->m_data>>offset<<offset) );
        }

    protected:
    private:
        std::bitset<cromosize> m_data;
};

#endif // MAXSQUARECROMOSOME_H

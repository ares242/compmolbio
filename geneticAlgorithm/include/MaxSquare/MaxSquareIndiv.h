#ifndef MAXSQUAREINDIV_H
#define MAXSQUAREINDIV_H

#include "AbstractIndividual.h"
#include "MaxSquareCromosome.h"

#include "common.h"

static std::uniform_int_distribution<> dis(0,pow(2,cromosize));

class MaxSquareIndiv : public AbstractIndividual
{
    public:
        MaxSquareIndiv()
        :AbstractIndividual(1)
        {}
        virtual ~MaxSquareIndiv(){}
        double evalFitness()
        {
            auto x = (dynamic_cast<MaxSquareCromosome*>(m_cromosomes[0]))->getData().to_ulong();
            return x*x;
        }

        void SetInitial()
        {
              std::bitset<cromosize> val(dis(gen));
              m_cromosomes[0] = new MaxSquareCromosome(val);
        }

        void mutate(){}

    protected:
    private:
};

#endif // MAXSQUAREINDIV_H

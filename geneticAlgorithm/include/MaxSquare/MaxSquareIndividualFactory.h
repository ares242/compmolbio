#ifndef MAXSQUAREINDIVIDUALFACTORY_H
#define MAXSQUAREINDIVIDUALFACTORY_H


class MaxSquareIndividualFactory : public AbstractIndividualFactory
{
    public:
        MaxSquareIndividualFactory() {}
        virtual ~MaxSquareIndividualFactory() {}

        AbstractIndividual * createIndividual()
        {
            return new MaxSquareIndiv;
        }

    protected:
    private:
};

#endif // MAXSQUAREINDIVIDUALFACTORY_H

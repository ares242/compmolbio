#ifndef MAXSQUARE_H
#define MAXSQUARE_H

#include "GeneticAlgorithm.h"
#include <bitset>
#include <random>
#include <vector>
#include <iostream>

#include "common.h"
#include "MaxSquareIndiv.h"
#include "MaxSquare/MaxSquareIndividualFactory.h"

class MaxSquare : public GeneticAlgorithm
{
    public:
        MaxSquare(size_t pop_size = 0,size_t iters=0)
        :GeneticAlgorithm(pop_size,iters,new MaxSquareIndividualFactory){}
        virtual ~MaxSquare(){}

    protected:
    private:
        void Mutation()
        {

        }

        void printPopulation(size_t generation)
        {
            std::vector<AbstractIndividual*>& old=m_old.Getindividuals();
            for(AbstractIndividual* & i:old)
            {
                MaxSquareIndiv* indiv = dynamic_cast<MaxSquareIndiv*>(i);
                std::cout<< ((MaxSquareCromosome *)(indiv->Getcromosomes()[0]))->getData().to_ulong()<<"\t"<<indiv->Getfitness()<<"\t"<<generation<<std::endl;
            }
        }
};

//std::max()

#endif // MAXSQUARE_H

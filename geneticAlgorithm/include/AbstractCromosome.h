#ifndef ABSTRACTCROMOSOME_H
#define ABSTRACTCROMOSOME_H

class AbstractCromosome
{
    public:
        AbstractCromosome(){};
        //Cromosome(T data){m_data = data;};
        virtual ~AbstractCromosome()
        {};

        /*T getData(){return m_data;}
        void setData(T val){m_data = val;}*/
        virtual AbstractCromosome * cross(AbstractCromosome * const & other) = 0;

    protected:
    private:
        //T m_data;
};

#endif // ABSTRACTCROMOSOME_H

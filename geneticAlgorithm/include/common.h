#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include <random>
#include <bitset>

typedef std::bitset<64> doubleBitSet;

static std::random_device rd;
static std::mt19937 gen(rd());
static std::uniform_real_distribution<double> prob_dis(0,1);

union Converter { uint64_t i; double d; };

double convert(doubleBitSet const& bs);
doubleBitSet convert(double bs);

#endif // COMMON_H_INCLUDED

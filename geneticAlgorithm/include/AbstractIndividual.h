#ifndef ABSTRACTINDIVIDUAL_H
#define ABSTRACTINDIVIDUAL_H

#include "AbstractCromosome.h"

#include <vector>
#include <cassert>
#include <iostream>

using std::size_t;



class AbstractIndividual
{
    public:
        AbstractIndividual(size_t num_cromos = 0)
        :m_cromosomes(std::vector<AbstractCromosome*>(num_cromos))
        {}

        /*Individual(Cromosome* cromo)
        :m_cromosome(cromo)
        {}*/
        /*Individual(T cromo)
        :m_cromosome(new Cromosome<T>(cromo))
        {}*/
        virtual ~AbstractIndividual()
        {
            for(auto & i : m_cromosomes)
                delete i;
        }

        double Getfenotipo() { return m_fenotipo; }
        void Setfenotipo(double val) { m_fenotipo = val; }
        double Getfitness() { return m_fitness; }
        void Setfitness(double val) { m_fitness = val; }
        double Getpselection() { return m_pselection; }
        void Setpselection(double val) { m_pselection = val; }

        std::vector<AbstractCromosome*>& Getcromosomes() { return m_cromosomes; }
        void Setcromosomes(std::vector<AbstractCromosome*> val) { m_cromosomes = val; }

        virtual AbstractIndividual* cross(AbstractIndividual* other)
        {
            size_t s = m_cromosomes.size();
            size_t s1 = other->m_cromosomes.size();
            assert(s==s1);
            for(size_t crom_i = 0; crom_i<s; crom_i++)
                m_cromosomes[crom_i] = m_cromosomes[crom_i]->cross(other->m_cromosomes[crom_i]);
            return this;
        }

        virtual double evalFitness() = 0;
        virtual void SetInitial() = 0;
        virtual void mutate() = 0;
        std::vector<AbstractCromosome*> m_cromosomes;

    protected:
        double m_fenotipo;
        double m_fitness;
        double m_pselection;
};
#endif // ABSTRACTINDIVIDUAL_H

#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <cmath>

using namespace std;

struct vector3d{
    double _x;
    double _y;
    double _z;
};

#define PI  3.141592653589793238462643383279502884

void getDescriptor( std::string const & pdbfilename, double const & a ){
    ifstream pdbfile(pdbfilename);
    if(!pdbfile.is_open()) return;
    std::vector<vector3d> alphacarbons;
    std::string command,atomname,buffer;
    size_t atomid;
    while(!pdbfile.eof()){
        pdbfile>>command;
        if(command=="ATOM")
        {
            pdbfile>>atomid>>atomname;
            if(atomname=="CA")
            {
                pdbfile.ignore(14-atomname.size());
                vector3d coords;
                pdbfile>>coords._x>>coords._y>>coords._z;
                alphacarbons.push_back(coords);
            }
        }
        pdbfile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }

    size_t totalca=alphacarbons.size();
    //computing distances
    std::vector<double> distances(totalca);
    std::vector<double> angles(totalca);
    std::vector<double> ans(totalca);
    distances[0] = 0;
    double max_d = 0;
    for(size_t i = 1; i<totalca;i++)
    {
        distances[i]=sqrt(pow(alphacarbons[i]._x-alphacarbons[i-1]._x,2)
                         +pow(alphacarbons[i]._y-alphacarbons[i-1]._y,2)
                         +pow(alphacarbons[i]._z-alphacarbons[i-1]._z,2));
        max_d = max(max_d,distances[i]);
    }
    angles[0] = 0;
    angles[totalca-1] = 0;
    for(size_t i = 1; i<totalca-1;i++)
    {
        double pp = (alphacarbons[i-1]._x-alphacarbons[i]._x)*(alphacarbons[i+1]._x-alphacarbons[i]._x)
                   +(alphacarbons[i-1]._y-alphacarbons[i]._y)*(alphacarbons[i+1]._y-alphacarbons[i]._y)
                   +(alphacarbons[i-1]._z-alphacarbons[i]._z)*(alphacarbons[i+1]._z-alphacarbons[i]._z);
        angles[i]=acos(pp/(distances[i]*distances[i+1]))/PI;
    }
    for(size_t i = 0; i<totalca;i++)
    {
        ans[i] = a*(distances[i]/max_d)+(1-a)*angles[i];
    }

    //for
    /*for(auto & c : alphacarbons){
        cout<<'('<<c._x<<','<<c._y<<','<<c._z<<')'<<endl;
    }
    cout<<"Num C-alpha atoms: "<<alphacarbons.size()<<endl;*/
    for(auto & d : ans)
    {
        cout<<d<<" ";
    }
    //cout<<endl;

}

int main()
{
    getDescriptor("../data/1NPX.pdb",0.5);
    //cout << "Hello World!" << endl;
    return 0;
}


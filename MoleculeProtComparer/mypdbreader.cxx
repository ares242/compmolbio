#include "mypdbreader.h"

#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkIdTypeArray.h"
#include "vtkStringArray.h"
#include "vtkTable.h"
#include "vtkIntArray.h"
#include <iterator>
#include <string>
#include <algorithm>
#include <sstream>

#include <iostream>

vtkStandardNewMacro(MouseInteractorStyle2)

myPDBReader::myPDBReader()
{
}

myPDBReader::~myPDBReader()
{
}

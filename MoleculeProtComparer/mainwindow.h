#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "MPComparer.h"
#include <vtkSmartPointer.h>
#include "mypdbreader.h"
#include <QVTKWidget.h>
#include <QLabel>
#include <QGridLayout>
#include <QTimer>
#include <QTimerEvent>
#include <vtkCamera.h>
#include <clickableqvtkwidget.h>

#define QUERYPROTEINFILE "Proteins500.txt"

namespace Ui {
class MainWindow;
}

const std::string PROTEINFILE = "Proteins.txt";

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(std::string const & datadir = "",QWidget *parent = 0);
    ~MainWindow();
    int readAndShowPDB(std::string const & proteinName,QVTKWidget * widget, bool cache = true);
    void showPDB(int index,QVTKWidget * widget);
    void showPDB(std::string pdbFileName,QVTKWidget * widget);
    void showPDB(std::string pdbFileName,ClickableQVTKWidget * widget);
    void showPDB(vtkSmartPointer<myPDBReader> const & pdb,QVTKWidget * widget, bool rotate = true);
    void showResult(MPComparer::myResult * result);
private slots:
    void on_proteinesListWidget_doubleClicked(const QModelIndex &index);
    void on_proteinesListWidget_currentRowChanged(int currentRow);

    void on_openPDBpushbutton_clicked();

    void on_range_pushbutton_clicked();

    void on_knearest_pushbutton_clicked();

    void on_result_tabWidget_tabCloseRequested(int index);
    void onresultVTKWidgetDblClick(ClickableQVTKWidget * src);

private:
    QWidget *client ;
    QGridLayout *gl ;
    Ui::MainWindow *ui;
    MPComparer * comparer;
    std::string datadir;
    std::vector<vtkSmartPointer<myPDBReader>> pdbCache;
    QLabel* statusLabel;
    size_t max_cols;
    size_t res_width,res_height;
    double alpha;
};

class RotateTimer: public QTimer
{
private:
    vtkSmartPointer<vtkCamera> camera;
    QVTKWidget * widget;
    double angle;
public:
    RotateTimer(vtkSmartPointer<vtkCamera> cam, QVTKWidget * widget, int msecs,double angle = 30){
        camera = cam;
        this->angle = angle;
        this->widget = widget;
        this->start(msecs);

    }
protected:
    void timerEvent(QTimerEvent * e){
        camera->Azimuth(angle);
        widget->update();
    }
};

#endif // MAINWINDOW_H

#include "clickableqvtkwidget.h"

#include <iostream>
#include <QMouseEvent>

ClickableQVTKWidget::ClickableQVTKWidget(QWidget *parent) :
    QVTKWidget(parent)
{
    counter = 0;
}

void ClickableQVTKWidget::mouseDoubleClickEvent(QMouseEvent * event)
{
    //std::cout<<"doubleclick "<<counter++<<endl;
    emit doubleClick(this);
}


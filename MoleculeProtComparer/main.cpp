#include "mainwindow.h"
#include <QApplication>
#include "protein.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    setlocale(LC_NUMERIC,"C");
    string datadir = "data/";
    MainWindow w(datadir);

    w.show();

    return a.exec();
}

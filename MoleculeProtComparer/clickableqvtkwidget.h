#ifndef CLICKABLEQVTKWIDGET_H
#define CLICKABLEQVTKWIDGET_H

#include <QVTKWidget.h>
#include <vtkSmartPointer.h>
#include <mypdbreader.h>

class ClickableQVTKWidget : public QVTKWidget
{
    Q_OBJECT
public:
    explicit ClickableQVTKWidget(QWidget *parent = 0);

signals:
    void doubleClick( ClickableQVTKWidget * src);
public slots:
    QString getName(){return name;}
    void setName(QString val){name = val;}

    vtkSmartPointer<myPDBReader> getPDB(){return pdb;}
    void setPDB(vtkSmartPointer<myPDBReader> const & val){pdb = val;}
protected:
    QString name;
    vtkSmartPointer<myPDBReader> pdb;
    size_t counter;
    // re-implement processing of mouse events
    //void mouseReleaseEvent ( QMouseEvent * e );
    //void mousePressEvent ( QMouseEvent * e );
    void mouseDoubleClickEvent(QMouseEvent * event);
};

#endif // CLICKABLEQVTKWIDGET_H

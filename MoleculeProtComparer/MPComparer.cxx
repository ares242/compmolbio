#include "MPComparer.h"
#include <sstream>
#include <iostream>

using namespace std;

struct vector3d{
    double _x;
    double _y;
    double _z;
};

#define PI  3.141592653589793238462643383279502884

void MPComparer::LoadTree(){
    ifstream in(treeFileName);
       char proteinName[200];
       double dWeights[SIZE];

       long w = 0;
       TProtein * protein;

       if (SlimTree!=NULL){
          if (in.is_open()){
             //cout << "\nLoading objects ";
             //while(in.getline(proteinName, 200, '\t'))
             string text;

             //while(in>>proteinName)
             while(getline(in,text,'\n'))
             {
                 stringstream ss;
                 ss<<text;
                 ss>>proteinName;

                for(size_t I=0;I<SIZE;++I)
                    ss>>dWeights[I];

                //in.ignore('\n');
                proteinNames.push_back(proteinName);
                protein = new TProtein(proteinName, dWeights);
                data[proteinName] = *protein;
                SlimTree->Add(protein);
                delete protein;
                w++;
                if (w % 10 == 0){
                   cout << '.';
                }//end if
             }//end while
             cout << " Added " << SlimTree->GetNumberOfObjects() << " objects "<<endl;
             in.close();
          }else{
             cout << "\nProblem to open the file."<<endl;
          }//end if
       }else{
          cout << "\n Zero object added!!"<<endl;
       }//end if
}

MPComparer::myResult* MPComparer::RangeQuery(stDistance radius){

   myResult * result = nullptr;
   //clock_t start, end;
   //unsigned int size;
   //unsigned int i,j;
   //TProtein * tmp;

   if (SlimTree){
      //size = queryObjects.size();
      // reset the statistics
      PageManager->ResetStatistics();
      SlimTree->GetMetricEvaluator()->ResetStatistics();
      //start = clock();
      //for (i = 0; i < size; i++){
         result = SlimTree->RangeQuery(queryObject, radius);
         cout<<"ResRQ: "<<result->GetNumOfEntries()<<endl;
         /* for (j = 0; j < result->GetNumOfEntries(); j++)
            {
                tmp = (TProtein *)(*result)[j].GetObject();
                cout<<tmp->GetName().c_str()<<endl;
            }
         delete result;
      }//end for
      end = clock();
      cout << "\nTotal Time: " << ((double )end-(double )start) / 1000.0 << "(s)";
      // is divided for queryObjects to get the everage
      cout << "\nAvg Disk Accesses: " << (double )PageManager->GetReadCount() / (double )size;
      // is divided for queryObjects to get the everage
      cout << "\nAvg Distance Calculations: " <<
         (double )SlimTree->GetMetricEvaluator()->GetDistanceCount() / (double )size;*/
   }//end if
    return result;
}

MPComparer::myResult* MPComparer::NearestQuery(stCount k)
{
   myResult * result = nullptr;
   /*clock_t start, end;
   unsigned int size;
   unsigned int i,j;
   TProtein * tmp;*/

   if (SlimTree){
      /*size = queryObjects.size();
      PageManager->ResetStatistics();
      SlimTree->GetMetricEvaluator()->ResetStatistics();
      start = clock();
      for (i = 0; i < size; i++){*/
         result = SlimTree->NearestQuery(queryObject, k);
         cout<<"ResNQ: "<<result->GetNumOfEntries()<<endl;
          /*for (j = 0; j < result->GetNumOfEntries(); j++)
            {
                tmp = (TProtein *)(*result)[j].GetObject();
                cout<<tmp->GetName().c_str()<<endl;
            }
         delete result;
      }//end for
      end = clock();
      cout << "\nTotal Time: " << ((double )end-(double )start) / 1000.0 << "(s)";
      // is divided for queryObjects to get the everage
      cout << "\nAvg Disk Accesses: " << (double )PageManager->GetReadCount() / (double )size;
      // is divided for queryObjects to get the everage
      cout << "\nAvg Distance Calculations: " <<
         (double )SlimTree->GetMetricEvaluator()->GetDistanceCount() / (double )size;*/
   }//end if
   return result;
}//end TApp::PerformNearestQuery

void MPComparer::getDescriptor( std::string const & pdbfilename, std::vector<double> & ans, double const & a )
{
    ifstream pdbfile(pdbfilename);
    if(!pdbfile.is_open()) return;
    std::vector<vector3d> alphacarbons;
    std::string command,atomname,buffer;
    size_t atomid;
    while(!pdbfile.eof()){
        pdbfile>>command;
        if(command=="ATOM")
        {
            pdbfile>>atomid>>atomname;
            if(atomname=="CA")
            {
                pdbfile.ignore(14-atomname.size());
                vector3d coords;
                pdbfile>>coords._x>>coords._y>>coords._z;
                alphacarbons.push_back(coords);
            }
        }
        pdbfile.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }

    size_t totalca=alphacarbons.size();
    //computing distances
    std::vector<double> distances(totalca);
    std::vector<double> angles(totalca);
    ans.resize(totalca);
    distances[0] = 0;
    double max_d = 0;
    for(size_t i = 1; i<totalca;i++)
    {
        distances[i]=sqrt(pow(alphacarbons[i]._x-alphacarbons[i-1]._x,2)
                         +pow(alphacarbons[i]._y-alphacarbons[i-1]._y,2)
                         +pow(alphacarbons[i]._z-alphacarbons[i-1]._z,2));
        max_d = max(max_d,distances[i]);
    }
    angles[0] = 0;
    angles[totalca-1] = 0;
    for(size_t i = 1; i<totalca-1;i++)
    {
        double pp = (alphacarbons[i-1]._x-alphacarbons[i]._x)*(alphacarbons[i+1]._x-alphacarbons[i]._x)
                   +(alphacarbons[i-1]._y-alphacarbons[i]._y)*(alphacarbons[i+1]._y-alphacarbons[i]._y)
                   +(alphacarbons[i-1]._z-alphacarbons[i]._z)*(alphacarbons[i+1]._z-alphacarbons[i]._z);
        angles[i]=acos(pp/(distances[i]*distances[i+1]))/PI;
    }
    for(size_t i = 0; i<totalca;i++)
    {
        ans[i] = a*(distances[i]/max_d)+(1-a)*angles[i];
    }

}

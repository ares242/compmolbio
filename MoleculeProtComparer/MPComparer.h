#ifndef MPCOMPARER_H
#define MPCOMPARER_H
#include <string>

#include <arboretum/stMetricTree.h>
#include <arboretum/stPlainDiskPageManager.h>
#include <arboretum/stDiskPageManager.h>
#include <arboretum/stMemoryPageManager.h>
#include <arboretum/stSlimTree.h>
#include <arboretum/stMetricTree.h>

#include <unordered_map>

// My object
#include "protein.h"

#include <string.h>
#include <fstream>
#include <vector>

class MPComparer{
public:
    typedef stResult < TProtein > myResult;
    typedef stMetricTree < TProtein, TProteinDistanceEvaluator > MetricTree;
    typedef stSlimTree < TProtein, TProteinDistanceEvaluator > mySlimTree;

    MPComparer(std::string treeFileName){
        this->treeFileName = treeFileName;
        PageManager = new stPlainDiskPageManager("SlimTree.dat", 2048);
        SlimTree = new mySlimTree(PageManager);
        LoadTree();
    }
    void LoadTree();

    static void getDescriptor(std::string const & pdbfilename, std::vector<double> & ans, double const & a);
    myResult* RangeQuery(stDistance radius);
    myResult* NearestQuery(stCount k);
    std::vector<string> getProteinNames(){
        return proteinNames;
    }

    void setQueryObjectbyName(string name){queryObject = &data[name];}

    TProtein * getQueryObject(){return queryObject;}
    void setQueryObject(TProtein * val){queryObject = val;}
private:

    stPlainDiskPageManager * PageManager;
    mySlimTree * SlimTree;
    TProtein * queryObject;
    std::vector<string> proteinNames;
    std::unordered_map<string,TProtein> data;
    std::string treeFileName;
};

#endif

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include <QVariant>
#include <QGridLayout>
#include <QScrollBar>
#include <QFileDialog>

#include <clickableqvtkwidget.h>

#include <cmath>
#include <vtkGlyph3D.h>
#include <vtkLODActor.h>
#include <vtkLODActor.h>
#include <vtkPDBReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTubeFilter.h>
#include <vtkInformation.h>
#include <vtkCellArray.h>
#include <vtkMolecule.h>
#include <vtkCamera.h>
#include <vtkRendererCollection.h>

MainWindow::MainWindow(std::string const & datadir,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    max_cols = 3;
    res_width = 400;
    res_height = 400;
    alpha = 0.5;
    ui->setupUi(this);
    this->datadir = datadir;
    statusLabel = new QLabel;
    this->statusBar()->addWidget(statusLabel);
    comparer = new MPComparer(datadir+PROTEINFILE);
    std::vector<std::string> && pnames = comparer->getProteinNames();
    for(std::string & pn : pnames)
    {
        ui->proteinesListWidget->addItem(QString::fromStdString(pn));
    }

    client = new QWidget(this);
    gl = new QGridLayout(client);

    client->setLayout(gl);
    ui->scrollArea->setWidget(client);
    //ui->scrollArea->horizontalScrollBar()->setSingleStep(client->width() / 24);

    /*for (int row = 0; row < 10; ++row) {
        for (int col = 0; col < 24; ++col) {
            QLabel *l = new QLabel(QString("R%1 C%2").arg(row).arg(col), client);
            l->setFixedSize(100, 40);  // makes the whole client widget abut 2400 px wide
            gl->addWidget(l, row, col);

        }
    }*/
}

MainWindow::~MainWindow()
{
    delete ui;
    delete comparer;
}

void MainWindow::on_proteinesListWidget_doubleClicked(const QModelIndex &index)
{
    //index.
    //QListWidgetItem cur = ui->proteinesListWidget->
}

void MainWindow::on_proteinesListWidget_currentRowChanged(int currentRow)
{
    QListWidgetItem* cur = ui->proteinesListWidget->currentItem();
    if(cur->data(Qt::UserRole).isNull())
    {
        statusLabel->setText("Loading pdb ...");
        std::string filedir = datadir+cur->text().toStdString()+".pdb";
        int pdbindex = readAndShowPDB(filedir,ui->mainqvtkwidget);
        cur->setData(Qt::UserRole,pdbindex);
        statusLabel->setText("Loading pdb ... Done");
    }
    else
    {
        statusLabel->setText("Rendering pdb ...");
        int pdbindex = cur->data(Qt::UserRole).toInt();
        showPDB(pdbindex,ui->mainqvtkwidget);
        statusLabel->setText("Rendering pdb ... Donde");
    }
    ui->mainqvtkwidget->setToolTip(cur->text());
    this->comparer->setQueryObjectbyName(cur->text().toStdString());
}

void MainWindow::showPDB(int index,QVTKWidget * widget)
{
    vtkSmartPointer<myPDBReader> & pdb = pdbCache[index];
    showPDB(pdb,widget);
}

void MainWindow::showPDB(std::string pdbFileName,QVTKWidget * widget)
{
    vtkSmartPointer<myPDBReader> pdb =
      vtkSmartPointer<myPDBReader>::New();

    pdb->SetFileName(pdbFileName.c_str());
    pdb->SetHBScale(1.0);
    pdb->SetBScale(1.0);
    pdb->Update();
    showPDB(pdb,widget);
}

void MainWindow::showPDB(std::string pdbFileName,ClickableQVTKWidget * widget)
{
    vtkSmartPointer<myPDBReader> pdb =
      vtkSmartPointer<myPDBReader>::New();

    pdb->SetFileName(pdbFileName.c_str());
    pdb->SetHBScale(1.0);
    pdb->SetBScale(1.0);
    pdb->Update();
    widget->setPDB(pdb);
    showPDB(pdb,widget,false);
}

int MainWindow::readAndShowPDB(std::string const & filedir,QVTKWidget * widget, bool cache)
{
    vtkSmartPointer<myPDBReader> pdb =
      vtkSmartPointer<myPDBReader>::New();

    pdb->SetFileName(filedir.c_str());
    pdb->SetHBScale(1.0);
    pdb->SetBScale(1.0);
    pdb->Update();
    showPDB(pdb,widget);

    if(cache)
    {
        pdbCache.push_back(pdb);
        return pdbCache.size()-1;
    }
    return -1;

}


void MainWindow::showPDB(vtkSmartPointer<myPDBReader> const & pdb,QVTKWidget * widget, bool rotate)
{
    widget->GetRenderWindow()->GetRenderers()->RemoveAllItems();
    vtkSmartPointer<vtkRenderer> mainrenderer = vtkSmartPointer<vtkRenderer>::New();
    mainrenderer->SetBackground(.1, .2, .3);
    double resolution = std::sqrt(300000.0 / pdb->GetNumberOfAtoms());
    if (resolution > 20)
      {
      resolution = 20;
      }
    if (resolution < 4)
      {
      resolution = 4;
      }
    //std::cout <<"Resolution is: " << resolution << std::endl;
    vtkSmartPointer<vtkSphereSource> sphere =
      vtkSmartPointer<vtkSphereSource>::New();
    sphere->SetCenter(0, 0, 0);
    //sphere->SetInformation();
    sphere->SetRadius(1);
    sphere->SetThetaResolution(static_cast<int>(resolution));
    sphere->SetPhiResolution(static_cast<int>(resolution));

    vtkSmartPointer<vtkGlyph3D> glyph =
      vtkSmartPointer<vtkGlyph3D>::New();
    glyph->SetInputConnection(pdb->GetOutputPort());
    glyph->SetOrient(1);
    glyph->SetColorMode(1);
    // glyph->ScalingOn();
    glyph->SetScaleMode(2);
    glyph->SetScaleFactor(.25);
    glyph->SetSourceConnection(sphere->GetOutputPort());

    vtkSmartPointer<vtkPolyDataMapper> atomMapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
    atomMapper->SetInputConnection(glyph->GetOutputPort());
    atomMapper->ImmediateModeRenderingOn();
    atomMapper->UseLookupTableScalarRangeOff();
    atomMapper->ScalarVisibilityOn();
    atomMapper->SetScalarModeToDefault();

    vtkSmartPointer<vtkLODActor> atom =
      vtkSmartPointer<vtkLODActor>::New();
    atom->SetMapper(atomMapper);
    atom->GetProperty()->SetRepresentationToSurface();
    atom->GetProperty()->SetInterpolationToGouraud();
    atom->GetProperty()->SetAmbient(0.15);
    atom->GetProperty()->SetDiffuse(0.85);
    atom->GetProperty()->SetSpecular(0.1);
    atom->GetProperty()->SetSpecularPower(30);
    atom->GetProperty()->SetSpecularColor(1, 1, 1);
    //atom->SetPickable();
    atom->SetNumberOfCloudPoints(30000);

    mainrenderer->AddActor(atom);

    vtkSmartPointer<vtkTubeFilter> tube =
      vtkSmartPointer<vtkTubeFilter>::New();
    tube->SetInputConnection(pdb->GetOutputPort());
    tube->SetNumberOfSides(static_cast<int>(resolution));
    tube->CappingOff();
    //tube->SetInformation(new vtkInformation("sssss"));
    tube->SetRadius(0.2);
    tube->SetVaryRadius(0);
    tube->SetRadiusFactor(10);

    vtkSmartPointer<vtkPolyDataMapper> bondMapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
    bondMapper->SetInputConnection(tube->GetOutputPort());
    bondMapper->ImmediateModeRenderingOn();
    bondMapper->UseLookupTableScalarRangeOff();
    bondMapper->ScalarVisibilityOff();
    bondMapper->SetScalarModeToDefault();

    vtkSmartPointer<vtkLODActor> bond =
      vtkSmartPointer<vtkLODActor>::New();
    bond->SetMapper(bondMapper);
    bond->GetProperty()->SetRepresentationToSurface();
    bond->GetProperty()->SetInterpolationToGouraud();
    bond->GetProperty()->SetAmbient(0.15);
    bond->GetProperty()->SetDiffuse(0.85);
    bond->GetProperty()->SetSpecular(0.1);
    bond->GetProperty()->SetSpecularPower(30);
    bond->GetProperty()->SetSpecularColor(1, 1, 1);
    bond->GetProperty()->SetDiffuseColor(1.0000, 0.8941, 0.70981);

    // Set the custom stype to use for interaction.
     /* vtkSmartPointer<MouseInteractorStyle2> style =
        vtkSmartPointer<MouseInteractorStyle2>::New();
      style->SetDefaultRenderer(renderer);*/

    mainrenderer->AddActor(bond);
    //vtkSmartPointer<vtkCamera> camera =  mainrenderer->GetActiveCamera();
        //camera->SetFocalPoint(0,0,0);

    //renderer->Clear();
    widget->GetRenderWindow()->AddRenderer(mainrenderer);
    widget->update();
    if(rotate){
        mainrenderer->ResetCamera();
        vtkSmartPointer<vtkCamera> camera = mainrenderer->GetActiveCamera();
        RotateTimer * timer = new RotateTimer(camera,widget,100);
    }

    //this->ui->qvtkWidgetLeft->GetInteractor()->SetInteractorStyle(style);

}

void MainWindow::onresultVTKWidgetDblClick(ClickableQVTKWidget * src){
    QVTKWidget * w = new QVTKWidget;
    showPDB(src->getPDB(),w);
    int index = ui->result_tabWidget->addTab(w,src->toolTip());
    ui->result_tabWidget->setCurrentIndex(index);
}

void MainWindow::showResult(MPComparer::myResult * result)
{
    ui->scrollArea->setWidget(nullptr);
    client->setLayout(nullptr);
    delete gl;
    delete client;
    client = new QWidget(this);
    gl = new QGridLayout(client);
    size_t size = result->GetNumOfEntries();
    size_t row = 0;
    for(size_t i = 0,col=0; i<size;i++,col++)
    {
        if(col==max_cols)
        {
            col=0;
            row++;
        }
        /*QLabel *l = new QLabel(QString("R%1 C%2").arg(row).arg(col), client);
        l->setFixedSize(100, 100);  // makes the whole client widget abut 2400 px wide
        gl->addWidget(l, row, col);*/
        TProtein * obj = (TProtein*)((*result)[i].GetObject());
        QString molname = QString::fromStdString(obj->GetName());
        ClickableQVTKWidget *vtkw = new ClickableQVTKWidget(client);

        vtkw->setFixedSize(res_width,res_height);
        vtkw->setToolTip(molname);
        gl->addWidget(vtkw, row, col);
        std::string filedir = datadir+obj->GetName()+".pdb";
        showPDB(filedir,vtkw);
        QObject::connect(vtkw, SIGNAL(doubleClick(ClickableQVTKWidget*)),
                              this, SLOT(onresultVTKWidgetDblClick(ClickableQVTKWidget*)));
        //cout<<col<<','<<row<<endl;

    }
    client->setLayout(gl);
    ui->scrollArea->setWidget(client);
}

void MainWindow::on_openPDBpushbutton_clicked()
{
    QString pdbfilename = QFileDialog::getOpenFileName(this,"Open pdb",".","Molecule Files (*.pdb)");
    if(pdbfilename.isNull()) return;
    showPDB(pdbfilename.toStdString(),ui->mainqvtkwidget);
    QFileInfo fileInfo(pdbfilename);
    string pdbname = fileInfo.fileName().toStdString();
    QFile::copy(pdbfilename,QString::fromStdString(datadir+pdbname));
    pdbname = pdbname.substr(0,pdbname.length()-4);
    ui->mainqvtkwidget->setToolTip(QString::fromStdString(pdbname));
    double dWeights[SIZE];
    std::vector<double> w;
    MPComparer::getDescriptor(pdbfilename.toStdString(),w,alpha);
    std::copy(w.begin(),w.begin()+SIZE,dWeights);

    comparer->setQueryObject(new TProtein(pdbname,dWeights));
}

void MainWindow::on_range_pushbutton_clicked()
{
    showResult(comparer->RangeQuery(ui->range_doubleSpinBox->value()));
}

void MainWindow::on_knearest_pushbutton_clicked()
{
    showResult(comparer->NearestQuery(ui->k_spinBox->value()));
}

void MainWindow::on_result_tabWidget_tabCloseRequested(int index)
{
    if(index==0) return;
    ui->result_tabWidget->removeTab(index);
}

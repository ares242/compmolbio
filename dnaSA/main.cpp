#include <iostream>
#include <sstream>
#include <random>
#include <CImg.h>
#include <tbb/tbb.h>
//#include <opencv2/opencv.hpp>
using namespace std;
using namespace cimg_library;

template<typename T>
class matrix2d
{
public:
    matrix2d(size_t width=0,size_t height=0,T* data = nullptr)
    :_width(width),_height(height),_data(data)
    {
    }
    inline void assign(size_t width=0,size_t height=0)
    {
        _width = width;
        _height = height;
        _data =  new T[width*height];
    }
    void fill(T val)
    {
        T* iter = _data;
        for(size_t d_i = 0; d_i<(_width*_height); d_i++,iter++)
            (*iter) = val;
    }
    T & operator()(size_t const & x, size_t const & y)
    {
        return _data[y*_width+x];
    }
    size_t width(){return _width;}
    size_t height(){return _height;}
private:
    size_t _width;
    size_t _height;
    T* _data;
};

typedef matrix2d<float> IntImg;
typedef CImg<unsigned char> Img;
//typedef cv::Mat MatImg;

ostream & operator<<(ostream & os, IntImg img)
{
    os<<'('<<img.width()<<'x'<<img.height()<<')'<<endl;
    for(size_t i = 0; i<img.height(); i++)
    {
        for(size_t j = 0; j<img.width(); j++)
        {
            os<<img(j,i)<<"\t";
        }
        os<<endl;
    }
    return os;
}

template <typename T>
inline T max(const T& a,const T& b,const T& c)
{
    return max(max(a,b),c);
}

inline int isMatch(char& ch1, char& ch2)
{
    return (ch1==ch2)?1:-1;
}

static char dna_chars [] = "AGTC";
static std::random_device rd;
static std::mt19937 gen(rd());
static std::uniform_int_distribution<> dis(0, 3);
static std::uniform_int_distribution<> size_dis(0, 500);
string randomDNAchain(size_t size_ = size_dis(gen))
{
    //std::stringstream out;
    std::string out;
    out.resize(size_);
    for(size_t i = 0; i<size_;i++)
        out.at(i)=dna_chars[dis(gen)];
    return out;
}



int align(string& str1, string& str2, IntImg& img)
{
    size_t length1 = str1.size()+1;
    size_t length2 = str2.size()+1;
    img.assign(length1,length2);
    img.fill(0);
    size_t str1_i, str2_i;
    for(str1_i = 0; str1_i<length1;str1_i++)
        img(str1_i,0) = ((int)str1_i)*(-2);

    for(str2_i = 0; str2_i<length2;str2_i++)
        img(0,str2_i) = ((int)str2_i)*(-2);

    for(str1_i = 1; str1_i<length1; str1_i++)
    {
        for(str2_i = 1; str2_i<length2; str2_i++)
        {
            img(str1_i, str2_i) = max(img(str1_i-1,str2_i)-2,
                                    img(str1_i,str2_i-1)-2,
                                    img(str1_i-1,str2_i-1)+isMatch(str1[str1_i-1],str2[str2_i-1]));
        }
    }
    return img(length1-1,length2-1);
}


int recursiveTBBAlign(string& str1, string& str2, int str1_i, int str2_i, IntImg & img)
{
    if(str1_i == 0 || str2_i == 0)
        return 0;
    int left,upper,diag,res;
    tbb::task_group g;
    g.run([&]{left = isnan(img(str1_i-1,str2_i))? recursiveTBBAlign(str1,str2,str1_i-1,str2_i,img):img(str1_i-1,str2_i);});
    g.run([&]{upper = isnan(img(str1_i,str2_i-1))? recursiveTBBAlign(str1,str2,str1_i,str2_i-1,img):img(str1_i,str2_i-1);});
    g.run([&]{diag = isnan(img(str1_i-1,str2_i-1))? recursiveTBBAlign(str1,str2,str1_i-1,str2_i-1,img):img(str1_i-1,str2_i-1);});
    g.wait();
    res = img(str1_i,str2_i) = max(left-2,upper-2,diag+isMatch(str1[str1_i-1],str2[str2_i-1]));
    return res;
}

int TBBalign(string& str1, string& str2, IntImg& img)
{
    size_t length1 = str1.size()+1;
    size_t length2 = str2.size()+1;
    img.assign(length1,length2);
    img.fill(NAN);
    return recursiveTBBAlign(str1,str2,length1-1,length2-1,img);
}

void printMatrix(IntImg & img)
{
    int str1_i, str2_i;
    for(str1_i = 0; str1_i<img.width(); str1_i++)
    {
        for(str2_i = 0; str2_i<img.height(); str2_i++)
        {
            cout<<img(str1_i,str2_i)<<"  ";
        }
        cout<<endl;
    }
}

void traceback(std::string const & str1 ,std::string const & str2,IntImg & img, std::string & out1, std::string & out2)
{
    size_t img_i, img_j;
    img_i = img.width()-1;
    img_j = img.height()-1;
    //size_t maxsize = max(img_i,img_j);
    std::stringstream _out1;
    std::stringstream _out2;
    _out1<<str1[img_i-1];
    _out2<<str2[img_j-1];
    while(img_i>0 && img_j>0)
    {
        int diag = img(img_i-1,img_j-1);
        int left = img(img_i-1,img_j);
        int upper = img(img_i,img_j-1);
        if(diag>=left && diag>=upper)
        {
            _out1<<str1[img_i-2];
            _out2<<str2[img_j-2];
            img_i--,img_j--;
        }
        else if(left>=upper)
        {
            _out1<<str1[img_i-2];
            _out2<<'_';
            img_i--;
        }
        else
        {
            _out1<<'_';
            _out2<<str2[img_j-2];
            img_j--;
        }
    }

    out1 = _out1.str();
    out2 = _out2.str();
    reverse(out1.begin(),out1.end());
    reverse(out2.begin(),out2.end());
}

string completeTraceback(size_t img_i,size_t img_j,string & str1 ,string & str2,IntImg & img,string result)
{
    //img_i = img.width()-1;
    //img_j = img.height()-1;
    size_t maxsize = max(img_i,img_j);
    stringstream out;
    while(img_i>0 && img_j>0)
    {
        int diag = img(img_i-1,img_j-1);
        int left = img(img_i-1,img_j);
        int upper = img(img_i,img_j-1);
        if(diag>=left && diag>=upper)
        {
            if(diag==left)
            {
                completeTraceback(img_i-1,img_j,str1,str2,img,out.str()+"_");
            }
            if(diag==upper)
            {
                completeTraceback(img_i,img_j-1,str1,str2,img,out.str()+"_");
            }
            out<<str1[img_i-1];
            img_i--,img_j--;
        }
        else if(left>upper)
        {
            out<<'_';
            img_i--;
        }
        else
        {
            out<<'_';
            img_j--;
        }
    }
    string str_ = out.str();
    reverse(str_.begin(),str_.end());
    return str_;
}

void graphCompleteTraceback(size_t img_i,size_t img_j,string & str1 ,string & str2,IntImg & img,string result)
{
    img_i = img.width()-1;
    img_j = img.height()-1;
    size_t maxsize = max(img_i,img_j);
    stringstream out;
    while(img_i>0 && img_j>0)
    {
        int diag = img(img_i-1,img_j-1);
        int left = img(img_i-1,img_j);
        int upper = img(img_i,img_j-1);
        if(diag>=left && diag>=upper)
        {
            if(diag==left)
            {
                completeTraceback(img_i-1,img_j,str1,str2,img,out.str()+"_");
            }
            if(diag==upper)
            {
                completeTraceback(img_i,img_j-1,str1,str2,img,out.str()+"_");
            }
            //out<<str1[img_i-1];

            img_i--,img_j--;
        }
        else if(left>upper)
        {
            //out<<'_';
            img_i--;
        }
        else
        {
            //out<<'_';
            img_j--;
        }
    }
    //string str_ = out.str();
    //reverse(str_.begin(),str_.end());
    //return str_;
}

void normalize(IntImg & img,Img & img2)
{
    //img2 = cv::Mat::zeros(img.width(),img.height(), CV_8UC1);
    /*img2.assign(img.width(),img.height());
    img2.fill(0);
    float minimum = img.min();
    float maximum = img.max()+abs(minimum);
    size_t img_i, img_j;
    for(img_i = 0; img_i<img.width(); img_i++)
    {
        for(img_j = 0; img_j<img.height(); img_j++)
        {
            img2(img_i,img_j) = ((img(img_i,img_j)+abs(minimum))/maximum)*255.0;
        }
    }*/
}

int main()
{
    tbb::task_scheduler_init init;
    //string str1 = "AACAATTCCGACTAC";//"CAGCACTTGGATTCTCGG";//"ACGTCATCA";//randomDNAchain(20);
    //string str2 = "ACTACCTCGC";//"CAGCGTGG";//"TAGTGTCA";//randomDNAchain(20);
    string str1 = randomDNAchain();
    string str2 = randomDNAchain();
    IntImg img;//,tbb_img,img2;
    //tbb::tick_count t0,t1;
    //t0 = tbb::tick_count::now();
    int score = align(str1,str2,img);
    //int score2 = align(str2,str1,img2);
    /*t1 = tbb::tick_count::now();
    cout<<"Time0 => "<<(t1-t0).seconds()<<endl;
    t0 = tbb::tick_count::now();*/
    /*int score1 = TBBalign(str1,str2,tbb_img);
    t1 = tbb::tick_count::now();
    cout<<"TBB Time1 => "<<(t1-t0).seconds()<<endl;*/

    cout<<"Lineal score: "<<score<<endl;
//    cout<<"TBB score: "<<score1<<endl;
    //printMatrix(img);
    //img.print("original");
    //Img outimg0, outimg1;
    string a_str1,a_str2;
    traceback(str1,str2,img,a_str1,a_str2);
    cout<<str1<<endl;
    cout<<str2<<endl<<endl;
    cout<<a_str1<<endl<<endl;
    cout<<a_str2<<endl;
    //cout<<img<<endl;
//    cout<<result<<endl;
//    cout<<result.size()<<endl;
  //  cout<<(result==str1)<<endl;

//    normalize(img,outimg0);

    //normalize(tbb_img,outimg1);

    //outimg.save("DNAaligment.png");
    //cv::namedWindow("tbbspectrum", cv::WINDOW_AUTOSIZE); // Create a window for display.
	//cv::imshow("tbbspectrum", outimg);
	//cv::waitKey(0);
	//img.display("dd");
	//outimg0.display(" ");
	//outimg1.display(" ");

    return 0;
}

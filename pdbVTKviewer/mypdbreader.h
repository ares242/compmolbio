#ifndef MYPDBREADER_H
#define MYPDBREADER_H
/*
class myPDBReader
{
public:
    myPDBReader();
};*/

#include "vtkIOGeometryModule.h" // For export macro
#include "vtkMoleculeReaderBase.h"


class VTKIOGEOMETRY_EXPORT myPDBReader : public vtkMoleculeReaderBase
{
public:
  vtkTypeMacro(myPDBReader,vtkMoleculeReaderBase)
  void PrintSelf(ostream& os, vtkIndent indent);
  vtkMolecule * getMolecule(){return Molecule;}
  static myPDBReader *New();

protected:
  myPDBReader();
  ~myPDBReader();

  void ReadSpecificMolecule(FILE* fp);

private:
  myPDBReader(const myPDBReader&);  // Not implemented.
  void operator=(const myPDBReader&);  // Not implemented.
};

#endif // MYPDBREADER_H

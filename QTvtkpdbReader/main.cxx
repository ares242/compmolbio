#include <QApplication>
#include "QTvtkpdbReaderMainWindow.h"
#include <QTextCodec>

int main( int argc, char** argv )
{
  // QT Stuff
  QApplication app( argc, argv );
  setlocale(LC_NUMERIC,"C");
  QTvtkpdbReaderMainWindow qtvtkpdbReaderMainWindow;
  qtvtkpdbReaderMainWindow.show();
  
  return app.exec();
}

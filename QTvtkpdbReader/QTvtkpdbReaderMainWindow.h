#ifndef SideBySideRenderWindowsQt_H
#define SideBySideRenderWindowsQt_H

#include <vtkPDBReader.h>
#include <QMainWindow>

namespace Ui{
class QtvtkpdbReaderMainWindow;
}

class QTvtkpdbReaderMainWindow : public QMainWindow
{
  Q_OBJECT
public:

  // Constructor/Destructor
  QTvtkpdbReaderMainWindow();
  ~QTvtkpdbReaderMainWindow() {}
private:
  Ui::QtvtkpdbReaderMainWindow * ui;
  QString pdbfilename;
  void renderPDB();
public slots:

  virtual void slotExit();
private slots:
  void on_actionOpenFile_triggered();
};

/*class myPDBReader : public vtkPDBReader
{
public:
    static myPDBReader * New (){return new myPDBReader;}
    vtkMolecule * getMolecule(){return Molecule;}
};*/

#endif

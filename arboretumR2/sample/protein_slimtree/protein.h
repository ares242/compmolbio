//---------------------------------------------------------------------------
// protein.h - Implementation of the User Layer
//
// This file implements the 2 classes required by the SlimTree Library User
// Layer.
//
//---------------------------------------------------------------------------
#ifndef proteinH
#define proteinH

#include <math.h>
#include <string>
#include <time.h>
#include <ostream>
#include <vector>
using namespace std;

// Metric Tree includes
#include <arboretum/stUserLayerUtil.h>
#include <arboretum/stTypes.h>
#include <arboretum/stUtil.h>

//---------------------------------------------------------------------------
// Class TProtein
//---------------------------------------------------------------------------
/**
* This class abstracts a city in a map. Each city has a name and a pair
* latitude/longitude.
*
* <P>In addition to data manipulation methods (such as GetLatitude(), GetName()
* and others), this class implements the stObject interface. This interface
* qualifies this object to be indexed by a metric tree implemented by GBDI
* SlimTree Library.
*
* <P>This interface requires no inheritance (because of the use of class
* templates in the Structure Layer) but requires the following methods:
*     - TProtein() - A default constructor.
*     - Clone() - Creates a clone of this object.
*     - IsEqual() - Checks if this instance is equal to another.
*     - GetSerializedSize() - Gets the size of the serialized version of this object.
*     - Serialize() - Gets the serialzied version of this object.
*     - Unserialize() - Restores a serialzied object.
*
* <P>Since the array which contains the serialized version of the object must be
* created and destroyed by each object instance, this class will hold this array
* as a buffer of the serialized version of this instance. This buffer will be
* created only if required and will be invalidated every time the object changes
* its values.
*
* <P>The serialized version of the object will be created as follows:<BR>
* <CODE>
* +----------+-----------+--------+<BR>
* | Size | Weights[] | Name[] |<BR>
* +----------+-----------+--------+<BR>
* </CODE>
*
* <P>Size and Weights are stored as doubles (2 64-bit IEEE floating point
* value) and Name[] is an array of chars with no terminator. Since Name[] has
* a variable size (associated with the name of the city), the serialized form
* will also have a variable number of bytes.
*
* @version 1.0
* @author Fabio Jun Takada Chino
*/
class TProtein{
   public:
      /**
      * Default constructor. It creates a city with no name and longitude and
      * latitude set to 0. This constructor is required by stObject interface.
      */
      TProtein(){
         Name = "";
         Size = 0;
         Weights = vector<double>();

         // Invalidate Serialized buffer.
         Serialized = NULL;
      }//end TProtein

      /**
      * Creates a new protein.
      *
      * @param name The name of the protein.
      * @param size Size.
      * @param weights Weights.
      */
      TProtein(const string name,size_t size,vector<double> weights){
         Name = name;
         //Size = size;
         Weights = weights;
         Size = Weights.size();

         // Invalidate Serialized buffer.
         Serialized = NULL;
      }//end TProtein

      /**
      * Destroys this instance and releases all associated resources.
      */
      ~TProtein(){

         // Does Serialized exist ?
         if (Serialized != NULL){
            // Yes! Dispose it!
            delete [] Serialized;
         }//end if
      }//end TProtein

      /**
      * Gets the weights of the protein.
      */
      vector<double> GetWeights(){
         return Weights;
      }//end GetWeights
      
      /**
      * Gets the size of the protein.
      */
      size_t GetSize(){
		  return Weights.size();
         //return Size;
      }//end GetSize

      /**
      * Gets the name of the protein.
      */
      const string & GetName(){
         return Name;
      }//end GetName

      // The following methods are required by the stObject interface.
      /**
      * Creates a perfect clone of this object. This method is required by
      * stObject interface.
      *
      * @return A new instance of TProtein wich is a perfect clone of the original
      * instance.
      */
      TProtein * Clone(){
         return new TProtein(Name, Size, Weights);
      }//end Clone

      /**
      * Checks to see if this object is equal to other. This method is required
      * by  stObject interface.
      *
      * @param obj Another instance of TProtein.
      * @return True if they are equal or false otherwise.
      */
      bool IsEqual(TProtein *obj){
         return (Weights == obj->GetWeights());
      }//end IsEqual

      /**
      * Returns the size of the serialized version of this object in bytes.
      * This method is required  by  stObject interface.
      */
      stSize GetSerializedSize(){
         return sizeof(double) + (sizeof(double) * Weights.size()) + Name.length();
      }//end GetSerializedSize

      /**
      * Returns the serialized version of this object.
      * This method is required  by  stObject interface.
      *
      * @warning If you don't know how to serialize an object, this methos may
      * be a good example.
      */
      const stByte * Serialize();

      /**
      * Rebuilds a serialized object.
      * This method is required  by  stObject interface.
      *
      * @param data The serialized object.
      * @param datasize The size of the serialized object in bytes.
      * @warning If you don't know how to serialize an object, this methos may
      * be a good example.
      */
      void Unserialize (const stByte *data, stSize datasize);
   private:
      /**
      * The name of the protein.
      */
      string Name;

      /**
      * Protein's weights.
      */
      vector<double> Weights;
      
      /**
      * Protein's size.
      */
      size_t Size;

      /**
      * Serialized version. If NULL, the serialized version is not created.
      */
      stByte * Serialized;
};//end TMapPoint

//---------------------------------------------------------------------------
// Class TProteinDistanceEvaluator
//---------------------------------------------------------------------------
/**
* This class implements a metric evaluator for TProtein instances. It calculates
* the distance between cities by performing a euclidean distance between city
* coordinates (I know it is not accurate but is is only a sample!!!).
*
* <P>It implements the stMetricEvaluator interface. As stObject interface, the
* stMetricEvaluator interface requires no inheritance and defines 2 methods:
*     - GetDistance() - Calculates the distance between 2 objects.
*     - GetDistance2()  - Calculates the distance between 2 objects raised by 2.
*
* <P>Both methods are defined due to optmization reasons. Since euclidean
* distance raised by 2 is easier to calculate, It will implement GetDistance2()
* and use it to calculate GetDistance() result.
*
* @version 1.0
* @author Fabio Jun Takada Chino
*/
class TProteinDistanceEvaluator : public stMetricEvaluatorStatistics{
   public:
      /**
      * Returns the distance between 2 cities. This method is required by
      * stMetricEvaluator interface.
      *
      * @param obj1 Object 1.
      * @param obj2 Object 2.
      */
      stDistance GetDistance(TProtein *obj1, TProtein *obj2){
         return sqrt(GetDistance2(obj1, obj2));
      }//end GetDistance

      /**
      * Returns the distance between 2 cities raised by the power of 2.
      * This method is required by stMetricEvaluator interface.
      *
      * @param obj1 Object 1.
      * @param obj2 Object 2.
      */
      stDistance GetDistance2(TProtein *obj1, TProtein *obj2){
		double d=0.0;
		double delta;
		vector<double> v1=obj1->GetWeights();
		vector<double> v2=obj2->GetWeights();
	
        UpdateDistanceCount(); // Update Statistics

		auto I=min(v1.size(),v2.size());
		for(size_t J=0;J<I;++J)
		{
			delta = v1[J] - v2[J];
			d+=delta*delta;
		}

         return d;
      }//end GetDistance2

};//end TProteinDistanceEvaluator

//---------------------------------------------------------------------------
// Output operator
//---------------------------------------------------------------------------
/**
* This operator will write a string representation of a city to an outputstream.
*/
ostream & operator << (ostream & out, TProtein & protein);

#endif //end myobjectH

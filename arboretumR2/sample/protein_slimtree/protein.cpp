//---------------------------------------------------------------------------
// city.cpp - Implementation of the User Layer
//
// In this file we have the implementation of TCity::Serialize(),
// TCity::Unserialize() and an output operator for TCity (which is not required
// by user layer).
//
// Authors: Marcos Rodrigues Vieira (mrvieira@icmc.sc.usp.br)
//         Fabio Jun Takada Chino (chino@icmc.sc.usp.br)
// Copyright (c) 2003 GBDI-ICMC-USP
//---------------------------------------------------------------------------
#pragma hdrstop
#include "protein.h"
#pragma package(smart_init)

//---------------------------------------------------------------------------
// Class TProtein
//---------------------------------------------------------------------------
/**
* Returns the serialized version of this object.
* This method is required  by  stObject interface.
* @warning If you don't know how to serialize an object, this methos may
* be a good example.
*/
const stByte * TProtein::Serialize(){
	double * d;

   // Is there a seralized version ?
   if (Serialized == NULL){
      // No! Lets build the serialized version.

      // The first thing we need to do is to allocate resources...
      Serialized = new stByte[GetSerializedSize()];

      // We will organize it in this manner:
      // +----------+-----------+--------+
      // | Size | Weights[] | Name[] |<BR>
      // +----------+-----------+--------+
      // So, write the Longitude and Latitude should be written to serialized
      // version as follows
      d = (double *) Serialized; // If you ar not familiar with pointers, this
                                 // action may be tricky! Be careful!
	  Size = Weights.size();
	  d[0] = (double) Size;
      for(size_t I=1;I<Size+1;++I)
      {
		  d[I]=Weights[I-1];
	  }

      // Now, write the name after the Size+1 doubles...
      memcpy(Serialized + sizeof(double) + (sizeof(double) * Weights.size()), Name.c_str(), Name.length());
   }//end if

   return Serialized;
}//end TProtein::Serialize

/**
* Rebuilds a serialized object.
* This method is required  by  stObject interface.
*
* @param data The serialized object.
* @param datasize The size of the serialized object in bytes.
* @warning If you don't know how to serialize an object, this methos may
* be a good example.
*/
void TProtein::Unserialize(const stByte *data, stSize datasize){
   double * d;
   stSize strl;

   // This is the reverse of Serialize(). So the steps are similar.
   // Remember, the format of the serizalized object is
   // +----------+-----------+--------+
   // | Size | Weights[] | Name[] |<BR>
   // +----------+-----------+--------+

   // Read Longitude and Latitude
   d = (double *) data;  // If you ar not familiar with pointers, this
                         // action may be tricky! Be careful!
   Size = (size_t) d[0];
   //Weights = vector<double>(d+1, d+1 + Size);
   Weights = vector<double>();
      for(size_t I=1;I<Size+1;++I)
      {
		  auto v=d[I];
		  Weights.push_back(v);
	  }

   // To read the name, we must discover its size first. Since it is the only
   // variable length field, we can get it back by subtract the fixed size
   // from the serialized size.
   strl = datasize - (sizeof(double) * (Size+1));

   // Now we know the size, lets get it from the serialized version.
   Name.assign((char *)(data + (sizeof(double) * (Size+1))), strl);

   // Since we have changed the object contents, we must invalidate the old
   // serialized version if it exists. In fact we, may copy the given serialized
   // version of tbe new object to the buffer but we don't want to spend memory.
   if (Serialized != NULL){
      delete [] Serialized;
      Serialized = NULL;
   }//end if
}//end TCity::Unserialize

//---------------------------------------------------------------------------
// Output operator
//---------------------------------------------------------------------------
/**
* This operator will write a string representation of a city to an outputstream.
*/
ostream & operator << (ostream & out, TProtein & protein){

   out << "[Protein=" << protein.GetName() << ", Size=" <<protein.GetSize() << "]";
   return out;
}//end operator <<


//---------------------------------------------------------------------------
// app.cpp - Implementation of the application.
//
// To change the behavior of this application, comment and uncomment lines at
// TApp::Init() and TApp::Query().
//
// Authors: Marcos Rodrigues Vieira (mrvieira@icmc.usp.br)
// Copyright (c) 2003 GBDI-ICMC-USP
//---------------------------------------------------------------------------
#include <iostream>
#include <sstream>
#pragma hdrstop
#include "app.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
// Class TApp
//------------------------------------------------------------------------------
void TApp::CreateTree(){
   // create for Slim-Tree
   SlimTree = new mySlimTree(PageManager);
}//end TApp::CreateTree

//------------------------------------------------------------------------------
void TApp::CreateDiskPageManager(){
   //for SlimTree
   PageManager = new stPlainDiskPageManager("SlimTree.dat", 32768);
}//end TApp::CreateDiskPageManager

//------------------------------------------------------------------------------
void TApp::Run(){
   // Lets load the tree with a lot values from the file.
   cout << "\n\nAdding objects in the SlimTree";
   LoadTree(PROTEINFILE);

   cout << "\n\nLoading the query file";
   LoadVectorFromFile(QUERYPROTEINFILE);

   if (queryObjects.size() > 0){
      // Do 500 queries.
      PerformQueries();
   }//end if
   // Hold the screen.
   cout << "\n\nFinished the whole test!";
}//end TApp::Run

//------------------------------------------------------------------------------
void TApp::Done(){

   if (this->SlimTree != NULL){
      delete this->SlimTree;
   }//end if
   if (this->PageManager != NULL){
      delete this->PageManager;
   }//end if

   // delete the vector of queries.
   for (unsigned int i = 0; i < queryObjects.size(); i++){
      delete (queryObjects.at(i));
   }//end for
}//end TApp::Done

//------------------------------------------------------------------------------
void TApp::LoadTree(char * fileName){
   ifstream in(fileName);
   char proteinName[200];
   double dWeights[SIZE];
   size_t size;

   long w = 0;
   TProtein * protein;

   if (SlimTree!=NULL){
      if (in.is_open()){
         cout << "\nLoading objects ";
         //while(in.getline(proteinName, 200, '\t'))
         string text;

         //while(in>>proteinName)
         while(getline(in,text,'\n'))
         {
			 stringstream ss;
			 ss<<text;
			 ss>>proteinName;
			 ss>>size;

            for(size_t I=0;I<size;++I)
				ss>>dWeights[I];
			
			for(size_t I=size;I<SIZE;++I)
				dWeights[I]=0;

            //in.ignore('\n');
            protein = new TProtein(proteinName,dWeights,size);
            SlimTree->Add(protein);
            delete protein;
            w++;
            if (w % 10 == 0){
               cout << '.';
            }//end if
         }//end while
         cout << " Added " << SlimTree->GetNumberOfObjects() << " objects ";
         in.close();
      }else{
         cout << "\nProblem to open the file.";
      }//end if
   }else{
      cout << "\n Zero object added!!";
   }//end if
}//end TApp::LoadTree

//------------------------------------------------------------------------------
void TApp::LoadVectorFromFile(char * fileName){
   ifstream in(fileName);
   char proteinName[200];
   int cont;
   double dWeights[SIZE];
   size_t size;

   // clear before using.
   queryObjects.clear();

   if (in.is_open()){
      cout << "\nLoading query objects ";
      cont = 0;
      
      string text;
      while(getline(in,text,'\n'))
      {
			 stringstream ss;
			 ss<<text;
			 ss>>proteinName;
			 ss>>size;

            for(size_t I=0;I<size;++I)
				ss>>dWeights[I];
			
			for(size_t I=size;I<SIZE;++I)
				dWeights[I]=0;

      /*while(in.getline(proteinName, 200, '\t')){
            for(size_t I=0;I<SIZE;++I)
				in>>dWeights[I];

		in.ignore(700,'\n');*/
        // in.ignore();
         this->queryObjects.insert(queryObjects.end(),new TProtein(proteinName,dWeights,size));
         cont++;
      }//end while
      cout << " Added " << queryObjects.size() << " query objects ";
      in.close();
   }else{
      cout << "\nProblem to open the query file.";
      cout << "\n Zero object added!!\n";
   }//end if
}//end TApp::LoadVectorFromFile

//------------------------------------------------------------------------------
void TApp::PerformQueries(){
   if(SlimTree)
   {
      /*cout << "\nStarting Statistics for Range Query with SlimTree.... ";
      PerformRangeQuery();
      cout << " Ok\n";*/

      cout << "\nStarting Statistics for Nearest Query with SlimTree.... ";
      PerformNearestQuery();
      cout << " Ok\n";
   }//end if
}//end TApp::PerformQuery

//------------------------------------------------------------------------------
void TApp::PerformRangeQuery(){

   myResult * result;
   stDistance radius;
   clock_t start, end;
   unsigned int size;
   unsigned int i,j;
   TProtein * tmp;

   if (SlimTree){
      size = queryObjects.size();
      // reset the statistics
      PageManager->ResetStatistics();
      SlimTree->GetMetricEvaluator()->ResetStatistics();
      start = clock();
      for (i = 0; i < size; i++){
         result = SlimTree->RangeQuery(queryObjects[i], 0.01);
         cout<<"ResRQ: "<<result->GetNumOfEntries()<<endl;
          for (j = 0; j < result->GetNumOfEntries(); j++)
			{
				tmp = (TProtein *)(*result)[j].GetObject();
				cout<<tmp->GetName().c_str()<<endl;
			}
         delete result;
      }//end for
      end = clock();
      cout << "\nTotal Time: " << ((double )end-(double )start) / 1000.0 << "(s)";
      // is divided for queryObjects to get the everage
      cout << "\nAvg Disk Accesses: " << (double )PageManager->GetReadCount() / (double )size;
      // is divided for queryObjects to get the everage
      cout << "\nAvg Distance Calculations: " <<
         (double )SlimTree->GetMetricEvaluator()->GetDistanceCount() / (double )size;
   }//end if

}//end TApp::PerformRangeQuery

//------------------------------------------------------------------------------
void TApp::PerformNearestQuery(){

   myResult * result;
   clock_t start, end;
   unsigned int size;
   unsigned int i,j;
   TProtein * tmp;

  TProteinDistanceEvaluator ev;

   if (SlimTree){
      size = queryObjects.size();
      PageManager->ResetStatistics();
      SlimTree->GetMetricEvaluator()->ResetStatistics();
      start = clock();
      for (i = 0; i < size; i++){
         result = SlimTree->NearestQuery(queryObjects[i], 5);
         cout<<"ResNQ: "<<result->GetNumOfEntries()<<endl;
          for (j = 0; j < result->GetNumOfEntries(); j++)
			{
				tmp = (TProtein *)(*result)[j].GetObject();
				cout<<tmp->GetName().c_str()<<", d="<<ev.GetDistance(queryObjects[i],tmp)<<endl;
			}
         delete result;
      }//end for
      end = clock();
      cout << "\nTotal Time: " << ((double )end-(double )start) / 1000.0 << "(s)";
      // is divided for queryObjects to get the everage
      cout << "\nAvg Disk Accesses: " << (double )PageManager->GetReadCount() / (double )size;
      // is divided for queryObjects to get the everage
      cout << "\nAvg Distance Calculations: " <<
         (double )SlimTree->GetMetricEvaluator()->GetDistanceCount() / (double )size;
   }//end if
}//end TApp::PerformNearestQuery
